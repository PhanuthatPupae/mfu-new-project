import React, { Component } from 'react'
import { Button, Dropdown, Avatar, Typography, Row, Col, Input, Icon, Menu, message } from 'antd'
import HeaderMain from '../components/general/HeaderMain'
import CKEditor from 'ckeditor4-react';
import axios from 'axios';

const { Title } = Typography;
const { TextArea } = Input;
export default class BlogPage extends Component {
    state = {
        user: [],
        imageUrl: '',
        title: '',
        detail: '',
        facebook: '',
        line: '',
        phone: '',
        blog: [],
        typeName: 'ประเภท',
        typeId: '',
        color: '',
        backgroundColor: ''
    }

    componentDidMount() {
        const jsonStr = localStorage.getItem('user-data');
        const user = jsonStr && JSON.parse(jsonStr)
        this.setState({ user: user })
        if (!jsonStr.imageUrl) {
            this.setState({ imageUrl: 'https://sv1.picz.in.th/images/2019/06/12/1tZYEN.png' })
        } else {
            this.setState({ imageUrl: jsonStr.imageUrl })
        }
        axios({
            url: `http://localhost:8080/blog/${this.props.location.state.id}`,
            method: 'get'
        }).then(res => {
            this.setState({ blog: res.data })
        })
    }

    menu = () => {
        return (
            <Menu>
                {
                    this.state.typeBlog.map(item => (
                        <Menu.Item onClick={() => this.setState({ typeName: item.typeName, typeId: item.id })}>{item.typeName}</Menu.Item>
                    ))
                }
            </Menu>
        )
    }

    onTitleChange = event => {
        const title = event.target.value;
        this.setState({ title });
    };

    onDetailChange = event => {
        const detail = event.target.value;
        this.setState({ detail });
    };

    checkItemFavorited = () => {
        const type = this.state.blog.favorites.map(item => {
            if (item.userData.id === this.state.user.id) {
                return '';
            } else {
                return 'danger';
            }
        })
        if (this.state.blog.favorites <= 0) {
            return 'danger'
        }
        return type;
    }

    changeBackgroundColor = () => {
        const backgroundColor = this.state.blog.favorites.map(item => {
            if (item.userData.id === this.state.user.id) {
                return 'red';
            } else {
                return '';
            }
        })
        return backgroundColor;
    }

    changeColor = () => {
        const color = this.state.blog.favorites.map(item => {
            if (item.userData.id === this.state.user.id) {
                return 'white';
            } else {
                return '';
            }
        })
        return color;
    }

    onClickFavorite = () => {
        this.state.blog.favorites.map(item => {
            if (item.userData.id === this.state.user.id) {
                axios({
                    url: `http://localhost:8080/unfavorite/${this.state.blog.id}/${item.id}`,
                    method: 'delete'
                }).then(res => {
                    this.setState({ blog: res.data })
                    message.success('เลิกถูกใจสำเร็จ', 1);
                })
            } else {
                axios({
                    url: `http://localhost:8080/add/favorite/${this.state.blog.id}/${this.state.user.id}`,
                    method: 'post',
                }).then(res => {
                    this.setState({ blog: res.data })
                    message.success('ถูกใจสำเร็จ', 1);
                })
            }
        })
        if (this.state.blog.favorites <= 0) {
            axios({
                url: `http://localhost:8080/add/favorite/${this.state.blog.id}/${this.state.user.id}`,
                method: 'post',
            }).then(res => {
                this.setState({ blog: res.data })
                message.success('ถูกใจสำเร็จ', 1);
            })
        }
    }

    onClickAddFriendButton = () => {
        axios({
            url: `http://localhost:8080/add/friend?userId=${this.state.user.id}&friendId=${this.state.blog.userData.id}`,
            method: 'post'
        }).then(res => {
            message.success('เพิ่มเพื่อนสำเร็จ', 1);
        }).catch(error => {
            message.error('เพิ่มเพื่อนไม่สำเร็จ', 1);
        });
    }

    onClickFollowButton = () => {
        axios({
            url: `http://localhost:8080/add/follower?userId=${this.state.user.id}&followerId=${this.state.blog.userData.id}`,
            method: 'post'
        }).then(res => {
            message.success('ติดตามสำเร็จ', 1);
        }).catch(error => {
            message.error('ติดตามไม่สำเร็จ', 1);
        });
    }

    render() {
        if (this.state.blog.userData) {
            return (
                <div>
                    <div>
                        <HeaderMain />
                    </div>
                    <div style={{ alignItems: 'center', flexDirection: 'column', display: 'flex' }}>
                        <div style={{ margin: 40, width: '80%' }}>
                            <div style={{ flexDirection: 'row', display: 'flex', justifyContent: 'center' }}>
                                <Title level={2} style={{ textAlign: "center" }}>เรื่อง: </Title>
                                <h2 style={{ marginLeft: 14, marginTop: 5 }}>{this.state.blog.title}</h2>
                                <div style={{ marginTop: 5, marginLeft: 20 }}>
                                    <Button>
                                        {this.state.blog.blogType.typeName}
                                    </Button>
                                </div>

                            </div>
                            <div style={{ wordSpacing: 2, textIndent: 80, textAlign: "left" }}>
                                <label>{this.state.blog.detail}</label>
                            </div>
                        </div>
                        <div style={{ width: '80%', justifyContent: 'flex-end', alignItems: 'center', display: 'flex', paddingRight: 50, marginBottom: 10 }}>
                            <Button
                                key="submit"
                                icon="heart"
                                type={this.checkItemFavorited()}
                                size="large"
                                shape="circle"
                                style={{ backgroundColor: this.changeBackgroundColor(), color: this.changeColor() }}
                                onClick={() => this.onClickFavorite()}
                            />
                            <h3 style={{ marginLeft: 10 }}>{this.state.blog.amountOfFavorites} คน</h3>
                        </div>
                        <div style={{ borderStyle: "solid", borderWidth: "1px", borderColor: "black", borderRadius: 50, justifyContent: "center", width: '65%', padding: 10 }}>
                            <div style={{ justifyContent: "flex-end", display: 'flex', marginRight: 50 }}>
                                {
                                    this.state.user.role === 'member' ?
                                        <Button
                                            style={{ borderRadius: 50, backgroundColor: "#4760DE", borderStyle: "solid", borderWidth: "1px", borderColor: "#4760DE", margin: 10 }}
                                            onClick={() => this.onClickFollowButton()}
                                        >
                                            <label style={{ color: "white" }}>ติดตาม</label>
                                        </Button> : null
                                }
                                {
                                    this.state.user.role === 'member' ?
                                        <Button
                                            style={{ borderRadius: 50, backgroundColor: "#D11414", borderStyle: "solid", borderWidth: "1px", borderColor: "#D11414", margin: 10 }}
                                            onClick={() => this.onClickAddFriendButton()}
                                        >
                                            <label style={{ color: "white" }}>เพิ่มเพื่อน</label>
                                        </Button> : null
                                }
                            </div>

                            <div style={{ width: "80%", margin: 50 }}>
                                <Row>
                                    <Col span={6}>
                                        <div style={{ marginLeft: 50 }}>
                                            {this.state.blog.userData.image === null ? <Avatar size={120} src="https://sv1.picz.in.th/images/2019/06/12/1tZYEN.png" /> : <Avatar size={120} src={this.state.blog.userData.image} />}
                                        </div>
                                    </Col>
                                    <Col span={6}>
                                        <div style={{ flexDirection: 'row', display: 'flex' }}>
                                            <Title level={4}>ชื่อผู้ใช้ : </Title><label style={{ fontSize: 18 }}>&nbsp; &nbsp;{this.state.blog.userData.name}</label>
                                        </div>
                                        <div style={{ flexDirection: 'row', display: 'flex' }}>
                                            <Title level={4}>ความถนัด : </Title><label style={{ fontSize: 18 }}>&nbsp; &nbsp;{this.state.blog.userData.aptitude}</label>
                                        </div>
                                        <div style={{ flexDirection: 'row', display: 'flex' }}>
                                            <Title level={4}>สิ่งที่สนใจ : </Title><label style={{ fontSize: 18 }}>&nbsp; &nbsp;{this.state.blog.userData.interested}</label>
                                        </div>
                                    </Col>
                                </Row>
                            </div>
                            <div style={{ textAlign: "center", alignItems: "center" }}>
                                <div style={{ margin: 10, marginTop: 50, justifyContent: "left", display: "flex", paddingLeft: 100 }} >
                                    <h2>ช่องทางการติดต่อ</h2>
                                </div>
                                <div style={{ justifyContent: "center", alignItems: "center", marginLeft: 100, marginBottom: 50 }}>
                                    <Row>
                                        <Col span={6}>
                                            <img src={require('../assets/images/Facebook-icon.png')} height={50} /> :
                                            <label style={{ marginTop: 5, marginLeft: 14, fontSize: 18 }}>{this.state.blog.userData.facebook}</label>
                                        </Col>
                                        <Col span={6}>
                                            <img src={require('../assets/images/line-icon.png')} height={50} />:
                                            <label style={{ marginTop: 5, marginLeft: 14, fontSize: 18 }}>{this.state.blog.userData.line}</label>
                                        </Col>
                                        <Col span={6}>
                                            <img src={require('../assets/images/phone-icon2.png')} height={50} /> :
                                            <label style={{ marginTop: 5, marginLeft: 14, fontSize: 18 }}>{this.state.blog.userData.phone}</label>
                                        </Col>
                                    </Row>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            )
        } else {
            return (
                <div></div>
            )
        }
    }
}
