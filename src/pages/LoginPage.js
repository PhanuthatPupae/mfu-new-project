import React, { Component } from 'react';
import imageSlide1 from '../assets/images/imageSlide-1.png'
import { Input, Button, message } from 'antd';
import Header from '../components/general/Header'
import { auth } from '../firebase';
import axios from 'axios';

class LoginPage extends Component {

    state = {
        isLoading: false,
        email: '',
        password: '',
        imageUrl: '',
        data: []
    }

    componentDidMount() {
        const jsonStr = localStorage.getItem('user-data');
        const isLoggedIn = jsonStr && JSON.parse(jsonStr).isLoggedIn;
        if (isLoggedIn) {
            this.navigateToMainPage();
        }
    }

    validateEmail(email) {
        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(String(email).toLowerCase());
    }

    navigateToMainPage = () => {
        this.props.history.push('/profile', {
            login: true
        })
    }

    navigateToRegisterPage = () => {
        this.props.history.push('/register')
    }

    saveInformationUser = data => {
        localStorage.setItem(
            'user-data',
            JSON.stringify({
                email: data.email,
                isLoggedIn: true,
                name: data.name,
                imageUrl: data.image,
                phone: data.phone,
                interested: data.interested,
                aptitude: data.aptitude,
                role: data.role,
                date: data.timestamp,
                count: data.count,
                id: data.id,
                gender: data.gerder,
                facebook: data.facebook,
                line: data.line
            })
        );
        this.setState({ isLoading: false });
        this.props.history.push('/post')
    }

    onEmailChange = event => {
        const email = event.target.value;
        this.setState({ email });
    };

    onPasswordChange = event => {
        const password = event.target.value;
        this.setState({ password });
    };

    onClickLoginButton = e => {
        this.setState({ isLoading: true });
        const email = this.state.email;
        const password = this.state.password;
        const isEailValid = this.validateEmail(email);
        if (isEailValid) {
            auth
                .signInWithEmailAndPassword(email, password)
                .then(({ user }) => {
                    axios({
                        url: 'http://localhost:8080/users/register',
                        method: 'post',
                        data: {
                            email: this.state.email,
                            password: this.state.password,
                        }
                    })
                })
                .catch(_ => {
                    this.setState({ isLoading: false });
                    message.error('Email or Password invalid!', 1);
                });
            axios({
                url: 'http://localhost:8080/login',
                method: 'post',
                data: {
                    email: this.state.email,
                    password: this.state.password,
                }
            }).then(res => {
                this.saveInformationUser(res.data)
            }).catch(_ => {
                this.setState({ isLoading: false });
                message.error('Login Fail!', 1);
            });
        } else {
            this.setState({ isLoading: false });
            message.error('Email or Password invalid!', 1);
        }
    };

    render() {
        return (
            <div>
                <div>
                    <Header />
                </div>
                <div style={{ flex: 1, alignItems: 'center', justifyContent: 'center', display: 'flex', height: 900 }}>
                    <div style={{ backgroundColor: '#f28c8c', height: 700, width: 1000, borderRadius: 80, textAlign: 'center' }}>
                        <div style={{ textAlign: 'center' }}>
                            <h1 style={{ fontSize: 50, margin: 40 }}>Login</h1>
                        </div>
                        <div style={{ alignItems: 'center', justifyContent: 'center', display: 'flex', flexDirection: 'column', margin: 10 }}>
                            <div style={{ width: '50%' }}>
                                <h1>Email</h1>
                            </div>
                            <div style={{ width: '50%' }}>
                                <Input
                                    placeholder="Enter Email"
                                    style={{ borderRadius: 5 }}
                                    size="large"
                                    onChange={this.onEmailChange}
                                />
                            </div>
                        </div>
                        <div style={{ alignItems: 'center', justifyContent: 'center', display: 'flex', flexDirection: 'column', margin: 10 }}>
                            <div style={{ width: '50%' }}>
                                <h1>Password</h1>
                            </div>
                            <div style={{ width: '50%' }}>
                                <Input.Password
                                    placeholder="Enter Password"
                                    style={{ borderRadius: 5 }}
                                    size="large"
                                    onChange={this.onPasswordChange}
                                />
                            </div>
                        </div>
                        <Button
                            size="large"
                            style={{ backgroundColor: '#2FEB3C', borderColor: '#2FEB3C', borderRadius: 50, height: 44, width: 120, marginTop: 30 }}
                            onClick={() => this.onClickLoginButton()}
                            loading={this.state.isLoading}
                        >
                            <label style={{ color: 'white' }}>Login</label>
                        </Button>
                        <div style={{ marginTop: 30 }}>
                            <h2>Don't have an account</h2>
                            <a onClick={() => this.navigateToRegisterPage()}><h2 style={{ fontWeight: 1000 }}>Sign Up</h2></a>
                        </div>
                    </div>
                </div>
            </div>
        )
    }

}

export default LoginPage;
