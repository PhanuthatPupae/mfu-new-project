import React, { Component } from 'react';
import imageSlide1 from '../assets/images/imageSlide-1.png'
import { Carousel, Button } from 'antd';
import Header from '../components/general/Header';
class HomePage extends Component {

    componentDidMount() {
        const jsonStr = localStorage.getItem('user-data');
        const isLoggedIn = jsonStr && JSON.parse(jsonStr).isLoggedIn;
        if (isLoggedIn) {
            this.navigateToMainPage();
        }
    }

    navigateToMainPage = () => {
        this.props.history.push('/post')
    }

    onClickPostButton = () => {
        this.props.history.push('/post')
    }

    render() {
        return (
            <div>
                <Header />
                <div style={{ display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
                    <div style={{ width: '50%' }}>
                        <Carousel autoplay>
                            <div>
                                <img src={require('../assets/images/imageSlide-2.png')} />
                            </div>
                            <div>
                                <img src={require('../assets/images/imageSlide-2.png')} />
                            </div>
                            <div>
                                <img src={require('../assets/images/imageSlide-3.jpg')} />
                            </div>
                            <div>
                                <img src={require('../assets/images/imageSlide-4.png')} />
                            </div>
                            <div>
                                <img src={require('../assets/images/imageSlide-5.png')} />
                            </div>
                        </Carousel>
                    </div>
                </div>
                <div>
                    <div style={{ marginLeft: 70, marginRight: 70, marginTop: 40, flexDirection: 'row', display: 'flex' }}>
                        <h1 style={{ fontSize: 36, color: '#666666', marginLeft: 140 }}>วิธีการใช้งาน</h1>
                        <div style={{ flex: 1, alignItems: 'center', justifyContent: 'flex-end', display: 'flex', paddingRight: 100 }}>
                            <Button
                                size="large"
                                style={{ backgroundColor: '#2FEB3C', borderRadius: 50, height: 44, width: 120 }}
                                onClick={() => this.onClickPostButton()}
                            >
                                <label style={{ color: 'white' }}>หน้าโพส</label>
                            </Button>
                        </div>
                    </div>
                    <div style={{ marginLeft: 70, marginRight: 70, marginTop: 40, flexDirection: 'row', display: 'flex' }}>
                        <img src={require('../assets/images/Man-Laptop.png')} height={220} style={{ marginLeft: 100 }} />
                        <div style={{ marginLeft: 100 }}>
                            <h1>ค้นหา Knowledge and User</h1>
                            <h2>ผู้ใช้สามารถค้าหาคำของknowledgeเเละผู้ใช้คนอื่นๆได้โดยการพิมพ์คำหรือชื่อของผู้ใช้ลงไปในช่องค้นหา</h2>
                        </div>
                    </div>
                </div>
            </div>
        )
    }

}

export default HomePage;
